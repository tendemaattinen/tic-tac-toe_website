<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link type="text/css" rel="stylesheet" href="stylesheet.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Rekisteröityminen | Tic-Tac-Toe</title>
    </head>
    <body>
        <?php
            //Check if there are session on, if not, shows login and register and if on, show logout
            if(isset($_SESSION['username'])) {
                echo "<ul id='navi'>
                        <li id='nimi'><a href='mainMenu.php'>Tic-Tac-Toe</a></li>
                        <li class='login'><form id='logOut' action='logOut.php' method='post'>
                            <input type='submit' value='Kirjaudu ulos' class='loginBut'>
                        </form></li>
                        <li class='login'><form id='ownPage' action='ownPage.php' method='post'>
                            <input type='submit' value='leaderboard' class='loginBut'>
                        </form></li>
                        <li class='login'><p id='error'>".$er."</p></li>
                        </ul><br><br>";
            } else {
                echo "<ul id='navi'>
                        <li id='nimi'><a href='mainMenu.php'>Tic-Tac-Toe</a></li>
                        <li class='login'><form id='newUser' action='newUserScreen.php' method='post'>
                            <input type='submit' value='Rekisteröidy' class='loginBut'>
                        </form></li>
                        <li class='login'><form action='login.php' method='post'>
                            <input type='text' name='user' placeholder='Käyttäjä' class='loginFo'>
                            <input type='password' name='pw' placeholder='Salasana' class='loginFo'>
                            <input type='submit' value='Kirjaudu sisään' class='loginBut'>
                        </form></li>
                        <li class='login'><p id='error'>".$er."</p></li>
                        </ul><br><br>";
            }
        ?>
        <br><br>
        <div id='reg'>
            <h1 id=nUser>Luo uusi käyttäjä:</h1>
            <?php
                //Gives error message to user
                if (isset($_GET['error'])){
                $error = $_GET['error'];
                if ($error === '1') {
                    echo '<p style="color:red">Salasanan täytyy olla yli 8 merkkiä mutta kuitenkin alle 256 merkkiä.</p>';
                } else if ($error === '2') {
                    echo '<p style="color:red">Salasanassa saa olla vain kirjaimia ja numeroita.</p>';
                } else if ($error === '3') {
                    echo '<p style="color:red">Salasanassa täytyy olla vähintään yksi iso kirjain, yksi pieni kirjain ja yksi numero.</p>';
                } else if ($error === '4') {
                    echo '<p style="color:red">Käyttäjänimi on jo käytössä.</p>';
                }
            }
            ?>
            <form action='newUser.php' method='post'>
                <input type='text' name='user' class='regFo' placeholder="Käyttäjänimi"><br>
                <input type='password' name='pw' class='regFo' placeholder="Salasana"><br>
                <input type='submit' value='Luo käyttäjä' class='regBut'>
            </form><br>
        </div>
    </body>
</html>
