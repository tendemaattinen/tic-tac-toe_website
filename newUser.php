<?php
    //Adds new user to database
    session_start();
    $user = $_POST['user'];
    $pass = $_POST['pw'];
    $counter = 0;

    class Tab extends RecursiveIteratorIterator { 
        function __construct($it) { 
            parent::__construct($it, self::LEAVES_ONLY); 
        }
        function current() {
            return parent::current();
        }
    }
    
    
    $servername = getenv('IP');
    $username = getenv('C9_USER');
    $password = "";
    $database = "c9";
    
    //Search if username is alraedy used
    try {
        $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare("SELECT username FROM users WHERE username='".$user."'"); 
        $stmt->execute();
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC); 
        foreach(new Tab(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) {
            $counter += 1;
            $conn = null;
            header('Location: newUserScreen.php?error=4');
        }
    }
    catch(PDOException $e) {
        $conn = null;
        echo "Error: " . $e->getMessage();
    }
    
    //Gives error if username is already used
    if ($counter !== 0) {
        $conn = null;
        header('Location: newUserScreen.php?error=4');
    } else {
    
    //Check if password meet the requiments
    $len = strlen($pass);
    //Have to be less than 256 and longer than 8
    if (($len < 256) && ($len > 8)) {
        //Can't include special characters
        if (preg_match("/^[a-öA-Ö0-9]*$/",$pass)) {
            //Have to include one big letter, one small letter and one number
            if (preg_match('/[A-Ö]+/', $pass) && preg_match('/[a-ö]+/', $pass) && preg_match('/[0-9]+/', $pass)) {
                //Hash
                $pass = password_hash($pass, PASSWORD_DEFAULT);
                //Username can't be empty
                if ($user === ""){
                    echo 'hei';
                    header('Location: ' . $_SERVER['HTTP_REFERER']);
                } else {
                    //Inserts new user to database
                    try {
                        $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        $sql = "INSERT INTO users (username, pwhash, admin)
                        VALUES ('$user', '$pass', 0)";
                        $conn->exec($sql);
                    }
                    catch(PDOException $e) {
                        echo $sql . "<br>" . $e->getMessage();
                    }
                    $conn = null;
                    header('Location: mainMenu.php?error=3');
                }
            } else {
                header('Location: newUserScreen.php?error=3');
            }
        } else {
            header('Location: newUserScreen.php?error=2');
        }
    } else {
        header('Location: newUserScreen.php?error=1');
    }}
?>