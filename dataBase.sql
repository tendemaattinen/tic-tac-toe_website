CREATE TABLE users
(uid int(6) AUTO_INCREMENT PRIMARY KEY,
username VARCHAR(100),
pwhash VARCHAR(100),
admin int(1));

CREATE TABLE winners
(name VARCHAR(50) PRIMARY KEY,
numberOfWinnings int(100));