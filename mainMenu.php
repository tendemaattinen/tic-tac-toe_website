<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link type="text/css" rel="stylesheet" href="stylesheet.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="mainMenu.js"></script>
        <title>Etusivu | Tic-Tac-Toe</title>
    </head>
    <body>
        <?php
            session_start();
            //Gives messages to user
            if(isset($_GET['error'])) {
                $err = $_GET['error'];
                if ($err === '1'){
                    $er = '*Käyttäjänimi ja salasana eivät täsmää';
                } else if ($err === '2') {
                    $er = '*Kirjautuminen onnistui';
                } else if ($err === '3'){
                    $er = '*Rekisteröinti onnistui';
                } else if ($err === '4'){
                    $er = '*Nimet vaihdettu onnistuneesti';
                }
            }//Check if there are session on, if not, shows login and register and if on, show logout
            if(isset($_SESSION['username'])) {
                    echo "<ul id='navi'>
                            <li id='nimi'><a href='mainMenu.php'>Tic-Tac-Toe</a></li>
                            <li class='login'><form id='logOut' action='logOut.php' method='post'>
                             <input type='submit' value='Kirjaudu ulos' class='loginBut'>
                            </form></li>
                            <li class='login'><form id='ownPage' action='leaderboard.php' method='post'>
                                <input type='submit' value='Tuloslista' class='loginBut'>
                            </form></li>
                            <li class='login'><p id='error'>".$er."</p></li>
                        </ul>";
                } else {
                    echo "<ul id='navi'>
                            <li id='nimi'><a href='mainMenu.php'>Tic-Tac-Toe</a></li>
                            <li class='login'><form id='newUser' action='newUserScreen.php' method='post'>
                            <input type='submit' value='Rekisteröidy' class='loginBut'>
                        </form></li>
                        <li class='login'><form action='login.php' method='post'>
                            <input type='text' name='user' placeholder='Käyttäjä' class='loginFo'>
                            <input type='password' name='pw' placeholder='Salasana' class='loginFo'>
                            <input type='submit' value='Kirjaudu sisään' class='loginBut'>
                        </form></li>
                        <li class='login'><p id='error'>".$er."</p></li>
                        </ul>";
                }
        ?>
        <h1 id='bigText'>Tic-Tac-Toe</h1>
        <div id='buttons'>
            <form action='PVP.php' method='post'>
                <input type='submit' value='Pelaaja vastaan Pelaaja' class='buttons'>
            </form>
            <form action='gameBot.php' method='post'>
                <input type='submit' value='Pelaaja vastaan Botti'class='buttons'>
            </form>
            <form action='renamePlayers.php' method='post'>
                <input type='submit' value='Nimeä pelaajat' class='buttons' id='renameP'>
            </form>
            <form action='leaderboard.php' method='post'>
                <input type='submit' value='Tuloslista' class='buttons'>
            </form>
        <!-- mainMenu.js draws here weather data from Open Weather API -->
        <div id='weather'>
        </div>
        </div>
    </body>
</html>