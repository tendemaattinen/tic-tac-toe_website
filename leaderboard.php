<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link type="text/css" rel="stylesheet" href="stylesheet.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="leaderboard.js"></script>
        <title>Tuloslista | Tic-Tac-Toe</title>
    </head>
    <body>
        <div>
            <?php
                session_start();
                //Check if there are session on, if not, shows login and register and if on, show logout
                if(isset($_SESSION['username'])) {
                    echo "<ul id='navi'>
                            <li id='nimi'><a href='mainMenu.php'>Tic-Tac-Toe</a></li>
                            <li class='login'><form id='logOut' action='logOut.php' method='post'>
                             <input type='submit' value='Kirjaudu ulos' class='loginBut'>
                            </form></li>
                            <li class='login'><form id='ownPage' action='mainMenu.php' method='post'>
                            <input type='submit' value='Takaisin etusivulle' class='loginBut'>
                            </form></li>
                        </ul>";
                } else {
                    echo "<ul id='navi'>
                            <li id='nimi'><a href='mainMenu.php'>Tic-Tac-Toe</a></li>
                            <li class='login'><form id='newUser' action='newUserScreen.php' method='post'>
                            <input type='submit' value='Rekisteröidy' class='loginBut'>
                        </form></li>
                        <li class='login'><form action='login.php' method='post'>
                            <input type='text' name='user' placeholder='Käyttäjä' class='loginFo'>
                            <input type='password' name='pw' placeholder='Salasana'class='loginFo'>
                            <input type='submit' value='Kirjaudu sisään' class='loginBut'>
                        </form></li>
                        </ul>";
                }
            ?>
        </div><br><br>
        <!-- leaderboard.js draws here table which contains toplist -->
        <div id='lb'>
            <form action='toPDF.php' method='post'>
                <input type='submit' value='Tallenna PDF' class='regBut' id='topdf'>
            </form>
        </div>
    </body>
</html>