<?php
    //Login functionality
    session_start();
    $user = $_POST['user'];
    $pass = $_POST['pw'];
    $counter = 0;
    
    class TableRowsss extends RecursiveIteratorIterator { 
        function __construct($it) { 
            parent::__construct($it, self::LEAVES_ONLY); 
        }
        function current() {
            return parent::current();
        }
    }
    
    $servername = getenv('IP');
    $username = getenv('C9_USER');
    $password = "";
    $database = "c9";
    //Search username from database
    try {
        $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare("SELECT username FROM users WHERE username='".$user."'"); 
        $stmt->execute();
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC); 
        foreach(new TableRowsss(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) {
            $counter += 1;
            $conn = null;
            header('Location: mainMenu.php?error=1');
        }
    }
    catch(PDOException $e) {
        $conn = null;
        echo "Error: " . $e->getMessage();
    }
    //If it won't find usename from database, gives error and go back to main page
    if ($counter === 0) {
        $conn = null;
        header('Location: mainMenu.php?error=1');
    //Checks if password matches to databases password
    } else {
        try {
            $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $conn->prepare("SELECT pwhash FROM users WHERE username='".$user."'"); 
            $stmt->execute();
            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC); 
            foreach(new TableRowsss(new RecursiveArrayIterator($stmt->fetchAll())) as $c=>$d) { 
                //Logs in if password match
                if (password_verify($pass, $d)) {
                    $_SESSION['username'] = $user;
                    $conn = null;
                    header('Location: mainMenu.php?error=2');
                //Gives error to user if password won't match
                } else {
                    $conn = null;
                    header('Location: mainMenu.php?error=1');
                }
            }
        }
        catch(PDOException $e) {
            $conn = null;
            echo "Error: " . $e->getMessage();
        }
    }
?>
