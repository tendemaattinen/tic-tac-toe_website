/* global $ */
$(document).ready(function(){
    //Search Lappeenrantas weather from Open Weathers API and show it in front page
    var url = 'http://api.openweathermap.org/data/2.5/weather?q=Lappeenranta,fi&units=metric&appid=8c2f5151d074fe5f55397d8a526d4fe4';
    $.getJSON(url,function(result){
        $("#weather").append("<p id='wea'>Sää ulkona</p>");
        $("#weather").append("<p id='wea'>Kaupunki: " + result.name + "</p>");
        $("#weather").append("<p id='wea'>Sää: " + result.weather[0].main + "</p>");
        $("#weather").append("<p id='wea'>Lämpötila: " + result.main.temp + "°С</p>");
    });
});