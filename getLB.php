<?php
    //Reads winners from datbase and makes table to leaderboard.php via ajax in leaderboard.js
    session_start();
    $counter = 0;
    $servername = getenv('IP');
    $username = getenv('C9_USER');
    $password = "";
    $database = "c9";
    //Reads winners from database and draws table of toplist
    try {
        echo '<table id="LBT" align="center">';
        echo '<tr id="LBTR"><td class="r1"><p class="lbt">Sija</p></td><td class="r2"><p class="lbt">Nimi</p></td><td class="r3"><p class="lbt">Voittomäärä</p></td>';
        $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare("SELECT name, numberOfWinnings FROM winners ORDER BY numberOfWinnings DESC");
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $counter += 1;
            $name = $row['name'];
            $numb = $row['numberOfWinnings'];
            echo '<tr class="LBTable">';
            echo '<td class="r1"><p class="lbt">'.$counter.'</p></td><td class="r2"><p class="lbt">'.$name.'</p></td><td class="r3"><p class="lbt">'.$numb.'</p></td>';
            echo '</tr>';
        }
        echo '</table>';
        $conn = null;
    }
    catch(PDOException $e) {
        $conn = null;
    }
?>