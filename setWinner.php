<?php
    //Sets winner into database
    session_start();
    $name = $_POST['name'];
    $counter = 0;
    
    class TableR extends RecursiveIteratorIterator { 
        function __construct($it) { 
            parent::__construct($it, self::LEAVES_ONLY); 
        }
        function current() {
            return parent::current();
        }
    }
    
    $servername = getenv('IP');
    $username = getenv('C9_USER');
    $password = "";
    $database = "c9";
    //Saves how many winnings winner have
    try {
        $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare("SELECT numberOfWinnings FROM winners WHERE name='".$name."'");
        $stmt->execute();
        $result = $stmt->setFetchMode(PDO::FETCH_ASSOC); 
        foreach(new TableR(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) {
            $counter += 1;
            $conn = null;
        }
    }
    catch(PDOException $e) {
        $conn = null;
    }
    //If winner won't found from database, he will be added there
    if ($counter === 0) {
        try {
            $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $conn->prepare("INSERT INTO winners (name, numberOfWinnings) VALUES ('".$name."',1)"); 
            $stmt->execute();
            $conn = null;
        }
        catch(PDOException $e) {
            $conn = null;
            echo $sql . "<br>" . $e->getMessage();
        }
    //If winner is found from database, update to his number of winnings
    } else if ($counter === 1) {
        //Search count of winnings
        try {
            $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $conn->prepare("SELECT numberOfWinnings FROM winners WHERE name='".$name."'");
            $stmt->execute();
            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC); 
            foreach(new TableR(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) {
                $numberWin = $v;
                $conn = null;
            }
            //Updates table
            try {
                $numberWin += 1;
                $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $stmt = $conn->prepare("UPDATE winners SET numberOfWinnings=".$numberWin." WHERE name='".$name."'"); 
                $stmt->execute();
                $conn = null;
            }
            catch(PDOException $e) {
                $conn = null;
                echo $sql . "<br>" . $e->getMessage();
            }
        }
        catch(PDOException $e) {
            $conn = null;
            echo $sql . "<br>" . $e->getMessage();
        }
    }
?>