<?php
    //Prints pdf file from winners using fpdf (http://www.fpdf.org/)
    require('fpdf.php');
    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetFont('Arial','B',16);
    
    $counter = 0;
    $servername = getenv('IP');
    $username = getenv('C9_USER');
    $password = "";
    $database = "c9";
    //Reads winners from database and draws table of toplist
    try {
        $pdf->Cell(40,10,'Voittotilastot',0,1);
        $pdf->Cell(40,10,'Sijoitus | Nimi | Voittoluku',0,1);
        $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare("SELECT name, numberOfWinnings FROM winners ORDER BY numberOfWinnings DESC");
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $counter += 1;
            $name = $row['name'];
            $numb = $row['numberOfWinnings'];
            $pdf->Cell(40,10,$counter.'. '.$name.' ('.$numb.')',0,1);
        }
        $pdf->Output();
        $conn = null;
    }
    catch(PDOException $e) {
        $conn = null;
    }
?>