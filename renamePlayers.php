<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link type="text/css" rel="stylesheet" href="stylesheet.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="//code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="crossorigin="anonymous"></script>
		<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
	   <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="script.js"></script>
    <title>Uudelleennimeäminen | Tic-Tac-Toe</title>
  </head>
  <body>
    <?php
      session_start();
      //Check if there are session on, if not, shows login and register and if on, show logout
      if(isset($_SESSION['username'])) {
          echo "<ul id='navi'>
                  <li id='nimi'><a href='mainMenu.php'>Tic-Tac-Toe</a></li>
                  <li class='login'><form id='logOut' action='logOut.php' method='post'>
                  <input type='submit' value='Kirjaudu ulos' class='loginBut'>
                  </form></li>
                  <li class='login'><form id='ownPage' action='leaderboard.php' method='post'>
                  <input type='submit' value='Tuloslista' class='loginBut'>
                  </form></li>
                  <li class='login'><p id='error'>".$er."</p></li>
                  </ul>";
      } else {
          echo "<ul id='navi'>
                <li id='nimi'><a href='mainMenu.php'>Tic-Tac-Toe</a></li>
                <li class='login'><form id='newUser' action='newUserScreen.php' method='post'>
                <input type='submit' value='Rekisteröidy' class='loginBut'>
                </form></li>
                <li class='login'><form action='login.php' method='post'>
                <input type='text' name='user' placeholder='Käyttäjä' class='loginFo'>
                <input type='password' name='pw' placeholder='Salasana' class='loginFo'>
                <input type='submit' value='Kirjaudu sisään' class='loginBut'>
                </form></li>
                <li class='login'><p id='error'>".$er."</p></li>
                </ul>";
      }

      
    ?>
    <br><br>
    <div id='reg'>
      <h1 id='nUser'>Vaihda pelaajien nimet</h1>
      <form action='renamePlayers2.php' method='post'>
        <input type='text' name='player1' placeholder='Pelaaja 1' class='regFo'><br>
        <input type='text' name='player2' placeholder='Pelaaja 2' class='regFo'><br>
        <input type='submit' valu='Nimeä pelaajat' class='regBut' value='Vaihda'>
      </form><br>
    </div>
  </body>
</html>