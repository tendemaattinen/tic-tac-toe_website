<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link type="text/css" rel="stylesheet" href="stylesheet.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="script.js"></script>
        <title>Pelaaja vs Pelaaja | Tic-Tac-Toe</title>
    </head>
    <body>
        <div>
            <?php
                session_start();
                //If user haven't choosen players names, this gives placeholder names to them
                if((!isset($_SESSION['player1'])) || (isset($_SESSION['username']))){
                    $_SESSION['player1'] = 'Pelaaja 1';
                } else if (!isset($_SESSION['player2'])) {
                    $_SESSION['player2'] = 'Pelaaja 2';
                }
                //Check if there are session on, if not, shows login and register and if on, show logout
                if(isset($_SESSION['username'])) {
                    echo "<ul id='navi'>
                            <li id='nimi'><a href='mainMenu.php' id='name'>Tic-Tac-Toe</a></li>
                            <li class='login'><form id='login' action='logOut.php' method='post'>
                             <input type='submit' value='Kirjaudu ulos' class='loginBut'>
                            </form></li>
                            <li class='login'><form id='ownPage' action='leaderboard.php' method='post'>
                            <input type='submit' value='Tuloslista' class='loginBut'>
                            </form></li>
                        </ul>";
                } else {
                    echo "<ul id='navi'>
                            <li id='nimi'><a href='mainMenu.php'>Tic-Tac-Toe</a></li>
                            <li class='login'><form id='newUser' action='newUserScreen.php' method='post'>
                            <input type='submit' value='Rekisteröidy' class='loginBut'>
                        </form></li>
                        <li class='login'><form action='login.php' method='post'>
                            <input type='text' name='user' placeholder='Käyttäjä' class='loginFo'>
                            <input type='password' name='pw' placeholder='Salasana' class='loginFo'>
                            <input type='submit' value='Kirjaudu sisään' class='loginBut'>
                        </form></li>
                        </ul>";
                }
            ?>
            <br><br><br>
            <!-- Draws dialog, which shows games result -->
            <div id="dialog">
                <p id='gameResult' class='gameRes'></p>
                <ul id='dia'>
                    <li class='dlog'><form id='newGame' action='mainMenu.php' method='post'>
                        <input type='submit' value='Etusivu' class='dBut'>
                    </form></li>
                    <li class='dlog'><form id='newGame' action='PVP.php' method='post'>
                        <input type='submit' value='Pelaaja vs Pelaaja' class='dBut'>
                    </form></li>
                    <li class='dlog'><form id='newGame' action='gameBot.php' method='post'>
                        <input type='submit' value='Pelaaja vs Botti' class='dBut'>
                    </form></li>
                </ul>
            </div>
            <!-- Draws tac-tac-toe table -->
            <div id="table">
                <table class='gameBoard'>
        			<tr>
        				<td id='1' class='GBTD'><p id ="1"></p></td>
        				<td id='2'class='GBTD'><p id ="2"></p></td>
        				<td id='3'class='GBTD'><p id ="3"></p></td>
        				<td id='10'class='GBTD'><p id ="10"></p></td>
        				<td id='11'class='GBTD'><p id ="11"></p></td>
        			</tr>
        			<tr>
        				<td id='4'class='GBTD'><p id ="4"></p></td>
        				<td id='5'class='GBTD'><p id ="5"></p></td>
        				<td id='6'class='GBTD'><p id ="6"></p></td>
        				<td id='12'class='GBTD'><p id ="12"></p></td>
        				<td id='13'class='GBTD'><p id ="13"></p></td>
        			</tr>
        			<tr>
        				<td id='7'class='GBTD'><p id ="7"></p></td>
        				<td id='8'class='GBTD'><p id ="8"></p></td>
        				<td id='9'class='GBTD'><p id ="9"></p></td>
        				<td id='14'class='GBTD'><p id ="14"></p></td>
        				<td id='15'class='GBTD'><p id ="15"></p></td>
        			</tr>
        			<tr>
        				<td id='16'class='GBTD'><p id ="16"></p></td>
        				<td id='17'class='GBTD'><p id ="17"></p></td>
        				<td id='18'class='GBTD'><p id ="18"></p></td>
        				<td id='19'class='GBTD'><p id ="19"></p></td>
        				<td id='20'class='GBTD'><p id ="20"></p></td>
        			</tr>
        			<tr>
        				<td id='21'class='GBTD'><p id ="21"></p></td>
        				<td id='22'class='GBTD'><p id ="22"></p></td>
        				<td id='23'class='GBTD'><p id ="23"></p></td>
        				<td id='24'class='GBTD'><p id ="24"></p></td>
        				<td id='25'class='GBTD'><p id ="25"></p></td>
        			</tr>
        		</table>
    		</div>
    		<!-- Error message -->
    		<p id='error2'></p><br><br>
    	</div>
    </body>
</html>
