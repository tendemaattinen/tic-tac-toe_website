/* global $ */
$(document).ready(function(){

  //Gets players names from getData.php
  var a = '';
  var pl1='';
  var pl2='';
  var Req = new XMLHttpRequest();
  Req.onload = function() {
    var g =  this.responseText;
    a = g;
    a = a.replace('[','');
    a = a.replace(']','');
    var find = '"';
    var re  = new RegExp(find, 'g');
    a = a.replace(re, '');
    a = a.split(',');
    pl1 = a[0];
    pl2 = a[1];
  };
  Req.open("get", "getPlayers.php", true);
  Req.send();

  //Return number of winner
  function checkWinner(val) {
    if(val === 'X'){
      return 1;
    } else if(val === 'O'){
      return 2;
    }
  }
  
  //Return number of winner
  function checkWinner(val) {
    if(val === 'X'){
      return 1;
    } else if(val === 'O'){
      return 2;
    }
  }
  
  //Returns true if there are winner, otherwise returns  false
  function chhh(array, nu1, nu2, nu3, nu4){
    if (array[nu1] === 'X' && array[nu2] === 'X' && array[nu3] === 'X' && array[nu4] === 'X'){
      return true;
    } else if (array[nu1] === 'O' && array[nu2] === 'O' && array[nu3] === 'O' && array[nu4] === 'O') {
      return true;
    } else {
      return false;
    }
  }
  
  //Checks if there are winner
  function check(arr) {
    if (chhh(arr, 0, 1, 2, 9)){
      var win = checkWinner(arr[0]);
      return win;
    } else  if (chhh(arr, 1, 2, 9, 10)){
      var win = checkWinner(arr[1]);
      return win;
    } else  if (chhh(arr, 3, 4, 5, 11)){
      var win = checkWinner(arr[3]);
      return win;
    } else  if (chhh(arr, 4, 5, 11, 12)){
      var win = checkWinner(arr[4]);
      return win;
    } else  if (chhh(arr, 6, 7, 8, 13)){
      var win = checkWinner(arr[6]);
      return win;
    } else  if (chhh(arr, 7, 8, 13, 14)){
      var win = checkWinner(arr[7]);
      return win;
    } else  if (chhh(arr, 15, 16, 17, 18)){
      var win = checkWinner(arr[15]);
      return win;
    } else  if (chhh(arr, 16, 17, 18, 19)){
      var win = checkWinner(arr[16]);
      return win;
    } else  if (chhh(arr, 20, 21, 22, 23)){
      var win = checkWinner(arr[20]);
      return win;
    } else  if (chhh(arr, 21, 22, 23, 24)){
      var win = checkWinner(arr[21]);
      return win;
    } else  if (chhh(arr, 0, 3, 6, 15)){
      var win = checkWinner(arr[0]);
      return win;
    } else  if (chhh(arr, 3, 6, 15, 20)){
      var win = checkWinner(arr[3]);
      return win;
    } else  if (chhh(arr, 1, 4, 7, 16)){
      var win = checkWinner(arr[1]);
      return win;
    } else  if (chhh(arr, 4, 7, 16, 21)){
      var win = checkWinner(arr[4]);
      return win;
    } else  if (chhh(arr, 2, 5, 8, 17)){
      var win = checkWinner(arr[2]);
      return win;
    } else  if (chhh(arr, 5, 8, 17, 22)){
      var win = checkWinner(arr[5]);
      return win;
    } else  if (chhh(arr, 9, 11, 13, 18)){
      var win = checkWinner(arr[9]);
      return win;
    } else  if (chhh(arr, 11, 13, 18, 23)){
      var win = checkWinner(arr[11]);
      return win;
    } else  if (chhh(arr, 10, 12, 14, 19)){
      var win = checkWinner(arr[10]);
      return win;
    } else  if (chhh(arr, 12, 14, 19, 24)){
      var win = checkWinner(arr[12]);
      return win;
    } else  if (chhh(arr, 0, 4, 8, 18)){
      var win = checkWinner(arr[0]);
      return win;
    } else  if (chhh(arr, 4, 8, 18, 24)){
      var win = checkWinner(arr[4]);
      return win;
    } else  if (chhh(arr, 3, 7, 17, 23)){
      var win = checkWinner(arr[3]);
      return win;
    } else  if (chhh(arr, 1, 5, 13, 19)){
      var win = checkWinner(arr[1]);
      return win;
    } else  if (chhh(arr, 10, 11, 8, 16)){
      var win = checkWinner(arr[10]);
      return win;
    } else  if (chhh(arr, 11, 8, 16, 20)){
      var win = checkWinner(arr[11]);
      return win;
    } else  if (chhh(arr, 12, 13, 17, 21)){
      var win = checkWinner(arr[12]);
      return win;
    } else  if (chhh(arr, 9, 5, 7, 15)){
      var win = checkWinner(arr[9]);
      return win;
    } else {
      return 0;
    }
  }
    //Checks if game ends draw by checking if there are not anymore zeros in array
    function drawCheck(array) {
    var counter = 0;
    for (var i = 0; i < array.length; i++) {
      if (array[i] === 0) {
        counter += 1;
      }
    }
    if (counter === 0){
      return true;
    } else {
      return false;
    }
  }

    //Dialog script for winner dialog
    $('#dialog').dialog({
      'modal': true,
      'autoOpen': false
    });
    
    //Returns random number from array
    function random(array) {
      var randa = gameBoard[Math.floor(Math.random() * gameBoard.length)];
      return randa;
    }
    
    // Player versus bot functionality
    var used = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    var gameBoard = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25];
    var player = 1;
    var mark = 'X';
    //Players turn
    $('td').click(function(){
        $('#error2').empty();
        var uss = $(this).closest('td').attr('id');
        //Checks if tile is used
        if (used[uss - 1] !== 0){
          $('#error2').append('<p id="error">Ruutu on jo käytetty</p>');
        //Draws mark to gameboard
        } else {
          $(this).append('<p id="' + uss +'">' + mark + '</p>');
          used[uss-1] = mark;
          //Checks if player 1 is a winner
          if (check(used) === 1){
            $('#gameResult2').append('<p>Voittaja: ' + pl1 +'</p>');
            var nam = 'name=' + pl1;
            //Sends winner to database with ajax
            $.ajax({
              type:'POST',
              url:'setWinner.php',
              data: nam,
              dataType: 'text',
              async: false,
              success:  function(data){
              }
            });
            $('#dialog').dialog('open');
            $('#table *').off();
          } else if (drawCheck(used)){
            $('#gameResult2').append('<p>Tasapeli</p>');
            $('#dialog').dialog('open');
            $('#table *').off();
          } else {
        }
        //Change player
        if (player === 1) {
          player = 2;
          mark = 'O';
        } else {
          player = 1;
          mark = 'X';
        }
      //Bots turn
      for (var i=0 ;i < gameBoard.length; i++){
        var rand = random(gameBoard);
        var ifUsed = used[rand - 1];
        if (ifUsed === 0) {
          $('#' + rand).append('<p id="' + rand +'">' + mark + '</p>');
          used[rand-1] = mark;
          $('#vaihtuu').empty();
          //Checks if bot is a winner
          if (check(used) === 2){
            $('#gameResult2').append('<p>Voittaja: Botti</p>');
            var nam = 'name=Botti';
            //Sends winner to database with ajax
            $.ajax({
              type:'POST',
              url:'setWinner.php',
              data: nam,
              dataType: 'text',
              async: false,
              success:  function(data){
              }
            });
            $('#dialog').dialog('open');
            $('#table *').off();
          } else if (drawCheck(used)){
            $('#gameResult2').append('<p>Tasapeli</p>');
            $('#dialog').dialog('open');
            $('#table *').off();
          }
          //Change player again
          if (player === 1) {
            player = 2;
            mark = 'O';
          } else {
            player = 1;
            mark = 'X';
          }
          break;
        } else {
        }
      }
    }
  });
});
